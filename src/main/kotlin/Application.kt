import arrow.core.Either
import arrow.core.handleError
import io.ktor.http.*
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.Writer

fun main() {
    embeddedServer(Netty, port = 8080) {
        install(StatusPages) {
            status(NotFound) { call, status ->
                call.respondText(text = "404: Page Not Found", status = status)
            }
        }
        routing {
            helloRoutes()
            get("/stream") {
                call.respondTextStreamer(Json, OK, simple())
            }
        }
    }.start(wait = true)
}


suspend fun ApplicationCall.respondTextStreamer(contentType: ContentType? = null, status: HttpStatusCode? = null, flow: Flow<String>) {
    suspend fun Writer.streamFlowOf(flow: Flow<String>) = coroutineScope { flow.collect { write(it); flush() } }
    respondTextWriter(contentType, status) { streamFlowOf(flow) }
}

suspend fun simple(): Flow<String> = flow {
    for (i in 'A'..'Z') { delay(250); emit("$i") }
}

private fun Routing.helloRoutes() {
    route("/hello/{name}") {
        get {
            doWork(context.parameters["name"]).map { context.respondText(it) }
                .handleError { context.respond(NotFound) }
        }
    }
}

suspend fun doWork(string: String?) = coroutineScope { Either.fromNullable(string) }